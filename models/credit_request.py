# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import ValidationError, UserError
from datetime import datetime
from dateutil.relativedelta import relativedelta


class RequestCredit(models.Model):
    _name = 'credit_workflow.credit_request'

    name = fields.Char(string="Nombre", default="New")
    partner_id = fields.Many2one('res.partner', string="Cliente", required=True)
    requested_amount = fields.Float(string="Monto solicitado")
    term = fields.Integer(string="Plazos", default=1, required=True)
    start_date = fields.Date(string="Fecha de inicio del crédito", required=True, default=datetime.today())
    end_date = fields.Date(string="Fecha de finalizacion del crédito", readonly=True, compute='get_end_date')
    annual_interest_rate = fields.Float(string="Tasa de interés anual")
    reference_rate = fields.Float(string="Tasa de referencia")
    spread = fields.Char(string="Spread")
    multiplo_moratorio = fields.Char(string="Múltiplo moratorio")
    interest_type = fields.Selection([
        ('fixed', 'Fija'),
        ('variable', 'Variable'),
    ], string="Tipo de tasa de interés", required=True)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('sent', 'Solicitud Enviada'),
        ('approved', 'Aprobado'),
        ('denied', 'Rechazado'),
        ('cancelled', 'Cancelado'),
    ], string="Estado", default="draft")
    term_unit = fields.Selection([
        ('weeks', 'Semanas'),
        ('biweeks', 'Quincenas'),
        ('months', 'Meses'),
        ('bimesters', 'Bimestres'),
        ('trimesters', 'Trimestres'),
        ('semesters', 'Semestres'),
        ('years', 'Años'),
    ], string="Unidad de plazos", default="months", required=True)
    amortization = fields.One2many(
        'credit_workflow.amortization',
        'credit_request_id',
        string="Tabla de Amortizacion"
    )
    credit_type = fields.Selection([
        ('type_1', 'Tipo 1'),
        ('type_2', 'Tipo 2'),
        ('type_3', 'Tipo 3'),
        ('type_4', 'Tipo 4'),
        ('type_5', 'Tipo 5'),
    ], string="Tipo de credito", required=True)

    @api.constrains('term')
    def validate_positive_value(self):
        for r in self:
            if r.term < 0:
                raise ValidationError("El número de plazos debe ser un número positivo.")

    @api.constrains('annual_interest_rate')
    def validate_positive_value(self):
        for r in self:
            if r.annual_interest_rate <= 0:
                raise ValidationError("El interés anual debe ser un número positivo mayor a cero.")

    @api.depends('start_date', 'term', 'term_unit')
    def get_end_date(self):
        for r in self:
            r.end_date = r.calculate_date(r.start_date, r.term, r.term_unit)

    def calculate_date(self, start_date, plazos, unidades_plazo):
        if unidades_plazo == 'weeks':
            return start_date + relativedelta(weeks=plazos)
        elif unidades_plazo == 'biweeks':
            return start_date + relativedelta(weeks=2 * plazos)
        elif unidades_plazo == 'months':
            return start_date + relativedelta(months=plazos)
        elif unidades_plazo == 'bimesters':
            return start_date + relativedelta(months=2 * plazos)
        elif unidades_plazo == 'trimesters':
            return start_date + relativedelta(months=3 * plazos)
        elif unidades_plazo == 'semesters':
            return start_date + relativedelta(months=6 * plazos)
        elif unidades_plazo == 'years':
            return start_date + relativedelta(years=plazos)
        else:
            return start_date

    def send_credit_request(self):
        for r in self:
            if r.state == 'draft':
                r.state = 'sent'

    def approve_credit_request(self):
        for r in self:
            if r.state == 'sent':
                r.state = 'approved'

    def deny_credit_request(self):
        for r in self:
            if r.state == 'sent':
                r.state = 'denied'

    def cancel_credit(self):
        for r in self:
            if r.state == 'approved':
                r.state = 'cancelled'

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('credit_workflow.credit_request')
        return super(RequestCredit, self).create(vals)

    def return_draft(self):
        for r in self:
            if r.state == 'sent':
                r.state = 'draft'

    def calcular_tabla(self):
        vals_list = []
        for r in self:
            # Primero vacia el campo amortization
            r.amortization = [(5, 0, 0)]
            # Dependiendo de unidades de plazo se llena la tabla
            if r.term_unit == 'months':
                c = r.requested_amount
                saldo_capital = c
                i = (r.annual_interest_rate / 100) / 12
                n = r.term
                pago = c * (i * ((1 + i) ** n)) / (-1 + ((1 + i) ** n))
                pago = round(pago, 2)
                for x in range(n):
                    # Llenar lista con diccionarios y al final se pasan en el for
                    fecha_pago = r.start_date + relativedelta(months=x + 1)
                    num_pago = x + 1
                    pago_intereses = round(saldo_capital * i, 2)
                    pago_capital = pago - pago_intereses
                    saldo_capital_despues_pago = saldo_capital - pago_capital
                    vals_dict = {
                        'fecha_pago': fecha_pago,
                        'numero_pago': num_pago,
                        'saldo_capital': saldo_capital,
                        'pago_capital': pago_capital,
                        'pago_intereses': pago_intereses,
                        'monto_pago': pago if x != n-1 else pago_capital + pago_intereses,
                        'saldo_capital_despues_pago': saldo_capital_despues_pago if x != n-1 else 0,
                    }
                    vals_list.append(vals_dict)
                    saldo_capital = saldo_capital - pago_capital
                r.amortization = [(0, 0, x) for x in vals_list]

    def unlink(self):
        for r in self:
            if r.state != 'draft':
                raise UserError("No puede borrar solicitudes que no se encuentren en estado borrador")
            else:
                return super().unlink()
