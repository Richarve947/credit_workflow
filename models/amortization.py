# -*- coding: utf-8 -*-
from odoo import _, api, fields, models

class Amortization(models.Model):
    _name = 'credit_workflow.amortization'

    numero_pago = fields.Integer(string="Pago No")
    saldo_capital = fields.Float(string="Sdo. Capital")
    pago_capital = fields.Float(string="Pago Capital")
    pago_intereses = fields.Float(string="Pago Intereses")
    monto_pago = fields.Float(string="Monto de pago")
    saldo_capital_despues_pago = fields.Float(string="Sdo. Capital despúes del pago")
    fecha_pago = fields.Date(string="Fecha de pago")
    credit_request_id = fields.Many2one(
        'credit_workflow.credit_request',
        string="Solicitud de credito"
    )
