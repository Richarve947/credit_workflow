# -*- coding: utf-8 -*-
{
    'name': 'credit_workflow',
    'version': '1.0',
    'description': '',
    'summary': '',
    'author': '',
    'website': '',
    'license': 'LGPL-3',
    'category': '',
    'depends': [
        'base',
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/ir_sequence.xml',
        'views/credit_request_view.xml',
        'views/menuitem.xml'
    ],
    #'auto_install': True,
    'application': True,
    'installable': True,
}
